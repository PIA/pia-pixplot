# Pia PixPlot

This repository is based in Yale's DH Lab [PixPlot](https://github.com/YaleDHLab/pix-plot) application and visualizes the collection Ernst Brunner from the photo archive of the Swiss Society for Folklore Studies.

## Prerequisite

This repos uses: 

- [Pyenv](https://github.com/pyenv/pyenv) for Python version management
- [Poetry](https://python-poetry.org/) for packaging and dependency management

## Run Local

- Clone this repository
- Place the `output` folder (get it from Max) inside `./phd_sgv12_pixplot/`
- `cd` into `phd_sgv12_pixplot`, this is were the PixPlot application lives
- run `python -m http.server 5000`
- Open your browser and go to `http://localhost:5000/output`

## Deployment

To deploy copy-paste the `/phd_sgv12_pixplot/output/` to a webserver.

## Start from Sratch

If you want to run PixPlot again, follow these steps:

- Install Python 3.7 with `pyenv install 3.7`
- Clone the pixplot repository `git clone https://github.com/YaleDHLab/pix-plot.git`
- `cd` into the directory and `poetry init`
- Specify `3.7` as python version 
- And  `https://github.com/yaledhlab/pix-plot/archive/master.zip`  as dependencies
- `poetry env use 3.7.16` creates a new venv
- run `poetry install`
- move some of the files into the package folder `phd_sgv12_pixplot`
- cd into that
- then, `poetry run pixplot --images "path/to/your/images"